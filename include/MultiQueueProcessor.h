#pragma once

#include "MultiQueue.h"
#include "LoadBalancer.h"
#include "BoundedQueue.h"

template<typename Key, typename Value>
class MultiQueueProcessor {
private:
	using Queue = BoundedQueue<Value>;
	using LoadBalancer = LoadBalancer<Key, Value>;

public:
	explicit MultiQueueProcessor(std::size_t maxCapacity, std::size_t maxThreads = std::thread::hardware_concurrency())
		: mMultiQueuePtr(std::make_shared<MultiQueue<Key, Value, Queue>>(maxCapacity)),
		mLoadBalancer(std::make_shared<LoadBalancer>(mMultiQueuePtr, maxThreads)) {
	}

	~MultiQueueProcessor() noexcept {
		StopProcessing();
	}

	void StopProcessing() noexcept {
		mLoadBalancer->stop();
		mLoadBalancer->join();
	}

	void Subscribe(Key const &key, std::shared_ptr<IConsumer<Key, Value>> consumer) {
		mLoadBalancer->subscribe(key, consumer);
	}

	void Unsubscribe(Key const &key) {
		mLoadBalancer->unsubscribe(key);
	}

	void Enqueue(Key const &key, Value &&value) {
		mMultiQueuePtr->push(key, std::move(value));
		mLoadBalancer->notify();
	}

	std::shared_ptr<Value> Dequeue(Key const &key) {
		return mMultiQueuePtr->tryPop(key);
	}

	void Finalize() {
		auto threadProc = [this]() {
			while (true) {
				if (mMultiQueuePtr->empty()) {
					StopProcessing();
					return;
				}

				std::this_thread::sleep_for(std::chrono::milliseconds(100));
			}
		};

		std::thread thread(threadProc);
		if (thread.joinable())
			thread.join();
	}

protected:
	std::shared_ptr<IMultiQueue<Key, Value>> mMultiQueuePtr;
	std::shared_ptr<ILoadBalancer<Key, Value>> mLoadBalancer;
};
