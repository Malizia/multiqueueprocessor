#pragma once

#include <memory>

#include "IQueue.h"
#include "IConsumer.h"

template <typename Key, typename Value>
class ILoadBalancer {
public:
	ILoadBalancer(ILoadBalancer const &) = delete;
	ILoadBalancer &operator=(ILoadBalancer const &) = delete;
	virtual ~ILoadBalancer() noexcept {}

	virtual void stop() noexcept = 0;
	virtual void join() noexcept = 0;
	virtual void notify() noexcept = 0;

	virtual void subscribe(Key const &key, std::shared_ptr<IConsumer<Key, Value>> consumer) = 0;
	virtual void unsubscribe(Key const &key) = 0;

protected:
	ILoadBalancer() = default;
};
