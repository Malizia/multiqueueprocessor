#pragma once
#include <memory>

template<typename Key, typename Value>
class IConsumer {
public:
	virtual ~IConsumer() noexcept {}
	virtual void consume(Key const &key, std::shared_ptr<Value> value) = 0;
};
