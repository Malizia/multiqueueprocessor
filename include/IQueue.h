#pragma once

#include <memory>

template <typename Value>
class IQueue {
public:
	IQueue(IQueue const &) = delete;
	IQueue &operator=(IQueue const &) = delete;
	virtual ~IQueue() noexcept {}

	virtual bool push(Value &&value) = 0;
	virtual std::shared_ptr<Value> tryPop() noexcept = 0;
	virtual std::size_t size() const noexcept = 0;
	virtual bool empty() const noexcept = 0;
	virtual std::size_t erase() noexcept = 0;

protected:
	IQueue() = default;
};
