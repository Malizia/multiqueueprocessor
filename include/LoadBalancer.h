#pragma once

#include <map>
#include <set>
#include <list>
#include <atomic>
#include <thread>
#include <vector>
#include <shared_mutex>

#include "IMultiQueue.h"
#include "ILoadBalancer.h"

template <typename Key, typename Value>
class LoadBalancer : public ILoadBalancer<Key, Value> {
public:
	using MultiQueuePtr = std::shared_ptr<IMultiQueue<Key, Value>>;
	using ConsumerPtr = std::shared_ptr<IConsumer<Key, Value>>;
	using QueuePtr = std::shared_ptr<IQueue<Value>>;

private:
	using RLock = std::shared_lock<std::shared_mutex>;
	using WLock = std::unique_lock<std::shared_mutex>;
	using ULock = std::unique_lock<std::mutex>;

	struct Subscription {
		bool mValid;
		Key mKey;
		ConsumerPtr mConsumer;

		Subscription()
			: mValid(false),
			mKey(),
			mConsumer(nullptr) {
		}

		Subscription(Key const &key, ConsumerPtr consumer)
			: mValid(consumer),
			mKey(key),
			mConsumer(consumer) {
		}

		bool valid() const noexcept {
			return mValid;
		}

	};

	using SubscriptionPtr = std::shared_ptr<Subscription>;

public:
	LoadBalancer(MultiQueuePtr multiQueuePtr, std::size_t maxThreads = std::thread::hardware_concurrency())
		: mMultiQueuePtr(multiQueuePtr),
		mExitFlag(false),
		mThreads(),
		mKeys(),
		mFreeSubscriptions(),
		mActiveSubscriptions(),
		mMaxThreads(maxThreads > 0 ? maxThreads : std::thread::hardware_concurrency()),
		mActiveThreads(0),
		mOverallSubscriptions(0),
		mIndex(0),
		mAccessMutex() {

		try {
			mThreads.reserve(mMaxThreads);
			for (std::size_t i = 0; i < mMaxThreads; ++i) {
				mThreads.emplace_back(std::thread(&LoadBalancer::process, this));
			}
		}
		catch (...) {
			stop();
			join();
			throw;
		}
	}

	LoadBalancer(LoadBalancer const &) = delete;
	LoadBalancer &operator=(LoadBalancer const &) = delete;

	~LoadBalancer() noexcept {
		stop();
		join();
	}

	virtual void stop() noexcept override {
		mExitFlag = true;
		mCondVariable.notify_all();
	}

	virtual void join() noexcept override {
		for (auto &th : mThreads) {
			if (th.joinable())
				th.join();
		}
	}

	virtual void notify() noexcept override {
		if (mActiveThreads < mOverallSubscriptions * 4 && mActiveThreads < mMaxThreads)
			mCondVariable.notify_one();
	}

	virtual void subscribe(Key const &key, std::shared_ptr<IConsumer<Key, Value>> consumer) override {
		if (!consumer)
			return;

		WLock lock(mAccessMutex);
		if (mKeys.count(key) > 0)
			return;

		ULock ulock(mCondVariableMutex);
		mKeys.insert(key);

		try {
			if (!mFreeSubscriptions.empty()) {
				auto place = mFreeSubscriptions.front();
				mFreeSubscriptions.pop_front();
				*place = Subscription(key, consumer);
				return;
			}

			mActiveSubscriptions.emplace_back(std::make_shared<Subscription>(key, consumer));
		}
		catch (...) {
			mKeys.erase(key);
			throw;
		}

		ulock.unlock();
		++mOverallSubscriptions;

		notify();
	}

	virtual void unsubscribe(Key const &key) override {
		WLock lock(mAccessMutex);
		if (mKeys.count(key) == 0)
			return;

		ULock ulock(mCondVariableMutex);
		mKeys.erase(key);

		auto iter = std::find_if(std::begin(mActiveSubscriptions), std::end(mActiveSubscriptions), [&key](auto ptr) {
			if (ptr && ptr->valid())
				return ptr->mKey == key;

			return false;
		});

		if (iter != std::end(mActiveSubscriptions)) {
			**iter = Subscription();
			mFreeSubscriptions.push_back(*iter);
		}

		--mOverallSubscriptions;
	}

private:
	Subscription getSubscription() noexcept {
		RLock lock(mAccessMutex);

		if (mKeys.empty())
			return Subscription();

		std::size_t const index = mIndex++ % mActiveSubscriptions.size();

		auto result = mActiveSubscriptions[index];
		if (result->valid())
			return Subscription(result->mKey, result->mConsumer);

		return Subscription();
	}

	void process() noexcept {
		++mActiveThreads;

		while (!mExitFlag) {
			if (mMultiQueuePtr->empty() || mKeys.empty()) {
				ULock lock(mCondVariableMutex);
					--mActiveThreads;
					mCondVariable.wait(lock, [this]() {return mExitFlag || (!mMultiQueuePtr->empty() && !mKeys.empty());});
					++mActiveThreads;
				lock.unlock();

				if (mExitFlag) {
					--mActiveThreads;
					return;
				}
			}

			auto subscription = getSubscription();
			if (subscription.valid()) {
				auto value = mMultiQueuePtr->tryPop(subscription.mKey);
				if (value) {
					try {
						subscription.mConsumer->consume(subscription.mKey, value);
					}
					catch (...) {
					}
				}
			}
		}

		--mActiveThreads;
	}

private:
	MultiQueuePtr mMultiQueuePtr;
	std::atomic<bool> mExitFlag;
	std::vector<std::thread> mThreads;
	std::set<Key> mKeys;
	std::list<SubscriptionPtr> mFreeSubscriptions;
	std::vector<SubscriptionPtr> mActiveSubscriptions;
	uint32_t mMaxThreads;
	std::atomic<uint32_t> mActiveThreads;
	std::atomic<uint32_t> mOverallSubscriptions;
	std::atomic<std::size_t> mIndex;
	std::shared_mutex mAccessMutex;

	std::condition_variable mCondVariable;
	std::mutex mCondVariableMutex;
};
