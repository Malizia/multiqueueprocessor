#pragma once

#include <map>
#include <atomic>
#include <shared_mutex>

#include "IMultiQueue.h"

template <typename Key, typename Value, typename Queue>
class MultiQueue : public IMultiQueue<Key, Value> {
private:
	using RLock = std::shared_lock<std::shared_mutex>;
	using WLock = std::unique_lock<std::shared_mutex>;

public:
	explicit MultiQueue(std::size_t maxCapacity)
		: mMaxCapacity(maxCapacity),
		mOverallSize(0),
		mSharedMutex(),
		mQueues() {
	}

	~MultiQueue() noexcept = default;
	MultiQueue(MultiQueue const &) = delete;
	MultiQueue &operator=(MultiQueue const &) = delete;

	bool size() const noexcept override {
		return mOverallSize;
	}

	bool empty() const noexcept override {
		return mOverallSize == 0;
	}

	void erase() noexcept override {
		WLock lock(mSharedMutex);

		for (auto &queue : mQueues)
			queue.second->erase();

		mOverallSize = 0;
	}

	bool push(Key const &key, Value &&value) override {
		auto queue = getQueue(key);
		auto const result = queue->push(std::move(value));
		if (result)
			++mOverallSize;

		return result;
	}

	std::shared_ptr<Value> tryPop(Key const &key) noexcept override {
		auto queue = findQueue(key);
		if (queue) {
			auto result = queue->tryPop();
			if (result)
				--mOverallSize;

			return result;
		}

		return std::shared_ptr<Value>();
	}

	std::size_t size(Key const &key) const noexcept override {
		auto queue = findQueue(key);
		if (queue)
			return queue->size();

		return 0;
	}

	bool empty(Key const &key) const noexcept override {
		auto queue = findQueue(key);
		if (queue)
			return queue->empty();

		return true;
	}

	void erase(Key const &key) noexcept override {
		auto queue = findQueue(key);
		if (queue)
			mOverallSize -= queue->erase();
	}

private:
	std::shared_ptr<Queue> getQueue(Key const &key) {
		auto queue = findQueue(key);
		if (queue)
			return queue;

		WLock lock(mSharedMutex);
		auto pair = mQueues.emplace(std::make_pair(key, std::make_shared<Queue>(mMaxCapacity)));
		return pair.first->second;
	}

	std::shared_ptr<Queue> findQueue(Key const &key) const noexcept {
		RLock lock(mSharedMutex);

		auto iter = mQueues.find(key);
		if (iter != std::end(mQueues))
			return iter->second;

		return std::shared_ptr<Queue>();
	}

private:
	std::size_t mMaxCapacity;
	std::atomic<std::size_t> mOverallSize;
	mutable std::shared_mutex mSharedMutex;
	std::map<Key, std::shared_ptr<Queue>> mQueues;
};

