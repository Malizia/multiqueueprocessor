#pragma once

#include <mutex>
#include <queue>
#include <atomic>

#include "IQueue.h"

template <typename Value>
class BoundedQueue : public IQueue<Value> {
private:
	class SpinLock {
	public:
		SpinLock() {
			lock_flag.clear();
		}

		bool try_lock() {
			return !lock_flag.test_and_set(std::memory_order_acquire);
		}

		void lock() {
			for (size_t i = 0; !try_lock(); ++i)
				if (i % 100 == 0) std::this_thread::yield();
		}

		void unlock() {
			lock_flag.clear(std::memory_order_release);
		}

	private:
		std::atomic_flag lock_flag;
	};

	using SyncObject = std::mutex;
	using Lock = std::lock_guard<SyncObject>;

public:
	explicit BoundedQueue(std::size_t maxCapacity)
		: mSyncObject(),
		mMaxCapacity(maxCapacity),
		mQueue(),
		mSize(0) {
	};

	~BoundedQueue() noexcept = default;
	BoundedQueue(BoundedQueue const &) = delete;
	BoundedQueue &operator=(BoundedQueue const &) = delete;

	bool push(Value &&value) override {
		std::shared_ptr<Value> ptr = std::make_shared<Value>(std::move(value));
		Lock lock(mSyncObject);
		if (mQueue.size() < mMaxCapacity) {
			mQueue.push(ptr);
			++mSize;
			return true;
		}

		return false;
	}

	std::shared_ptr<Value> tryPop() noexcept override {
		Lock lock(mSyncObject);

		if (mQueue.empty())
			return std::shared_ptr<Value>();

		auto ptr = mQueue.front();
		mQueue.pop();
		--mSize;
		return ptr;
	}

	std::size_t size() const noexcept override {
		return mSize;
	}

	bool empty() const noexcept override {
		return mSize == 0;
	}

	std::size_t erase() noexcept override {
		Lock lock(mSyncObject);

		std::size_t const size = mSize;
		while (!mQueue.empty())
			mQueue.pop();

		mSize = 0;

		return size;
	}

private:
	mutable SyncObject mSyncObject;
	std::size_t mMaxCapacity;
	std::queue<std::shared_ptr<Value>> mQueue;
	std::atomic<std::size_t> mSize;
};
