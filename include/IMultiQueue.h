#pragma once

#include "IQueue.h"

template <typename Key, typename Value>
class IMultiQueue {
public:
	IMultiQueue(IMultiQueue const &) = delete;
	IMultiQueue &operator=(IMultiQueue const &) = delete;
	virtual ~IMultiQueue() noexcept {}

	virtual bool size() const noexcept = 0;
	virtual bool empty() const noexcept = 0;
	virtual void erase() noexcept = 0;

	virtual bool push(Key const &key, Value &&value) = 0;
	virtual std::shared_ptr<Value> tryPop(Key const &key) noexcept = 0;
	virtual std::size_t size(Key const &key) const noexcept = 0;
	virtual bool empty(Key const &key) const noexcept = 0;
	virtual void erase(Key const &key) noexcept = 0;

protected:
	IMultiQueue() = default;
};

