#pragma once

#include <cmath>
#include <atomic>
#include "../include/IConsumer.h"

template <typename Key, typename Value>
class FastConsumer : public IConsumer<Key, Value> {
public:
	FastConsumer()
		: mCounter(0) {
	}

	virtual void consume(Key const& key, std::shared_ptr<Value> value) override {
		value->mDouble = fibonnaci(value->mUint32 % 1400);
		value->mString = std::to_string(value->mDouble);
		++mCounter;
	}

	uint32_t getCounter() const noexcept {
		return mCounter;
	}

private:
	double fibonnaci(uint32_t n) const noexcept {
		static double const sqrt5 = std::sqrt(5.0);
		static double const phi = (1.0 + sqrt5) / 2.0;
		double const result = std::floor(std::pow(phi, n) / sqrt5 + 0.5);
		return result;
	}

private:
	std::atomic<uint32_t> mCounter;
};
