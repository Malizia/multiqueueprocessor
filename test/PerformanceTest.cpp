#include "PerformanceTest.h"

#include <Windows.h>
#include <gtest/gtest.h>

static Key GetKey() {
	static std::atomic<uint32_t> key = 0;
	return std::to_string(key++);
}

static void TestSPSC(State &state, uint32_t threads, uint32_t count) {
	DWORD const start = GetTickCount();
	auto const key = GetKey();

	auto mqp = std::make_shared<MQP>(count, threads);
	auto consumer = std::make_shared<Consumer>();
	mqp->Subscribe(key, consumer);

	{
		auto producer = std::make_shared<Producer>(mqp, key, count);
	}

	mqp->Finalize();

	ASSERT_EQ(consumer->getCounter(), count);

	DWORD const total = GetTickCount() - start;
	state.counters["consumes/sec"] = (double) consumer->getCounter() * 1000 / total;
}

static void TestMPSC(State &state, uint32_t threads, uint32_t count, uint32_t producers) {
	DWORD const start = GetTickCount();
	Key const key = GetKey();

	auto mqp = std::make_shared<MQP>(count * producers, threads);
	auto consumer = std::make_shared<Consumer>();
	mqp->Subscribe(key, consumer);

	{
		std::vector<std::shared_ptr<Producer>> allProducers;
		for (std::size_t i = 0; i < producers; ++i)
			allProducers.emplace_back(std::make_shared<Producer>(mqp, key, count));
	}

	mqp->Finalize();

	ASSERT_EQ(consumer->getCounter(), count * producers);

	DWORD const total = GetTickCount() - start;
	state.counters["consumes/sec"] = (double) consumer->getCounter() * 1000 / total;
}

static void TestMPMC(State &state, uint32_t threads, uint32_t count, uint32_t producersConsumers) {
	DWORD const start = GetTickCount();
	std::vector<Key> keys;
	for (uint32_t i = 0; i < producersConsumers; ++i)
		keys.emplace_back(GetKey());

	auto mqp = std::make_shared<MQP>(count * producersConsumers, threads);

	std::vector<std::shared_ptr<Consumer>> allConsumers;
	for (auto const& key : keys) {
		auto consumer = std::make_shared<Consumer>();
		allConsumers.push_back(consumer);
		mqp->Subscribe(key, consumer);
	}

	{
		std::vector<std::shared_ptr<Producer>> allProducers;
		for (auto const& key : keys)
			allProducers.emplace_back(std::make_shared<Producer>(mqp, key, count));
	}

	mqp->Finalize();

	uint32_t overallConsumption = 0;
	for (auto consumer : allConsumers)
		overallConsumption += consumer->getCounter();

	ASSERT_EQ(overallConsumption, count * producersConsumers);

	DWORD const total = GetTickCount() - start;
	state.counters["consumes/sec"] = (double) overallConsumption * 1000 / total;
}

BENCHMARK_F(PerformanceTest, Consumes)(State& state) {
	auto consumer = std::make_shared<Consumer>();
	auto const key = GetKey();
	uint32_t counter = 0;

	while (state.KeepRunning()) {
		auto value = std::make_shared<Value>(counter++);
		consumer->consume(key, value);
	}
}

BENCHMARK_F(PerformanceTest, Subscriptions)(State& state) {
	auto mqp = std::make_shared<MQP>(100);
	auto consumer = std::make_shared<Consumer>();

	while (state.KeepRunning()) {
		auto const key = GetKey();
		mqp->Subscribe(key, consumer);
		mqp->Unsubscribe(key);
	}
}

BENCHMARK_F(PerformanceTest, SPSC_1_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestSPSC(state, 1, 1000000);
	}
}

BENCHMARK_F(PerformanceTest, SPSC_2_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestSPSC(state, 2, 1000000);
	}
}

BENCHMARK_F(PerformanceTest, SPSC_4_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestSPSC(state, 4, 1000000);
	}
}

BENCHMARK_F(PerformanceTest, SPSC_8_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestSPSC(state, 8, 1000000);
	}
}

BENCHMARK_F(PerformanceTest, SPSC_max_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestSPSC(state, 0, 1000000);
	}
}

BENCHMARK_F(PerformanceTest, MP10SC_1_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestMPSC(state, 1, 1000000, 10);
	}
}

BENCHMARK_F(PerformanceTest, MP10SC_2_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestMPSC(state, 2, 1000000, 10);
	}
}

BENCHMARK_F(PerformanceTest, MP10SC_4_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestMPSC(state, 4, 1000000, 10);
	}
}

BENCHMARK_F(PerformanceTest, MP10SC_8_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestMPSC(state, 8, 1000000, 10);
	}
}

BENCHMARK_F(PerformanceTest, MP10SC_max_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestMPSC(state, 0, 1000000, 10);
	}
}

BENCHMARK_F(PerformanceTest, MP50MC50_max_1000000)(State& state) {
	while (state.KeepRunning()) {
		TestMPMC(state, 0, 1000000, 50);
	}
}

