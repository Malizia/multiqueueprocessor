#pragma once

#include <memory>
#include <thread>
#include "../include/MultiQueueProcessor.h"

template <typename Key, typename Value>
class SimpleProducer{
private:
	using MQP = MultiQueueProcessor<Key, Value>;
public:
	SimpleProducer(std::shared_ptr<MultiQueueProcessor<Key, Value>> mqp, Key const &key, uint32_t count)
		: mMQP(mqp),
		mKey(key),
		mCount(count),
		mThread(std::thread(&SimpleProducer::produce, this)) {
	}

	~SimpleProducer() noexcept {
		if (mThread.joinable())
			mThread.join();
	}

private:
	void produce() {
		for (uint32_t i = 0; i < mCount; ++i)
			mMQP->Enqueue(mKey, Value(i));
	}

private:
	std::shared_ptr<MQP> mMQP;
	Key mKey;
	uint32_t mCount;
	std::thread mThread;
};
