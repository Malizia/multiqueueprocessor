#pragma once

#include <mutex>
#include <atomic>
#include <condition_variable>
#include <benchmark/benchmark.h>

using State = ::benchmark::State;

#include "FastConsumer.h"
#include "SimpleProducer.h"
#include "../include/MultiQueueProcessor.h"

struct Stub {
	uint32_t mUint32;
	double mDouble;
	std::string mString;

	Stub(uint32_t value) : mUint32(value) {}
	Stub(double value) : mUint32(0), mDouble(value), mString() {}
	Stub(std::string const &value) : mUint32(0), mDouble(0), mString(value) {}
};

using Key = std::string;
using Value = Stub;
using MQP = MultiQueueProcessor<Key, Value>;
using Consumer = FastConsumer<Key, Value>;
using Producer = SimpleProducer<Key, Value>;

class PerformanceTest : public ::benchmark::Fixture {
public:
	void SetUp(State const &state) {
	}

	void TearDown(State const &state) {
	}

	Key GetKey() const {
		static std::atomic<uint32_t> key = 0;
		return std::to_string(key++);
	}
};
